<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'columbus');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '3Ky@syOo^QClqs rD[>D0]-7Sh}U18Lu$BN>rm@u<0DE}NTSB|$EV`o}_G$7yB%P');
define('SECURE_AUTH_KEY',  'V~SL>,sEcNSlG0_O]Oh;*|0h]oz&~na>(Pf?B9Wt.cy(3ZuTQkPjk~gyo#H;&q(W');
define('LOGGED_IN_KEY',    'BRU3C,?:!t^6uWb(>9**sa9Bkh0ChC=!UakD_&EdDDJ/H$5G8$0r3w|~clc92(Zj');
define('NONCE_KEY',        'Q[09Wll}UaQ>v,VmaN,K8j}OTW,/6uMSR7!E0I;K%L? $R$L:^XHgCk-4$Z,Sb{X');
define('AUTH_SALT',        'eooRtDy=aQ|:heAl@a#<:,>iZ6j4h)1<=/y$+9s0q~l#v(JwtaXv|dXKmA?,l)OT');
define('SECURE_AUTH_SALT', '(2U)O2M]-cp.[>)g?LY+Tk=eLSy|+/gv$)TX4M/}[%^i%,K$:QlhBRB63OVs%|9j');
define('LOGGED_IN_SALT',   '9$bAeE.@ON5-UbK6;~|3^hctv~HJeHdu_?I;&QTN(3v~qrOe&DhOg++}]mxeBB_`');
define('NONCE_SALT',       'c(B0uq*56B5oXtVGp2}s:81,5!K?$~{iUz3+FG| g6- 5bI$9sAQ4 ,6.98hg1%-');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
